<?php
include_once("markdown.php");
$dir = ($_GET["d"]) ? dirname(__FILE__) . "/" . $_GET["d"] : dirname(__FILE__);
$fname = ($_GET["f"]) ? $_GET["f"] : "index.md";
$file = file("$dir/$fname");
$title = $file[1];
$date = strtotime($file[2]);
?>

<html>
<head>
	<title><?php echo("$title"); ?></title>
</head>
<body>
	<?php
	echo(date("F j, Y", $date) . ": $dir/$fname<br>");
	echo(Markdown(implode("", $file)));
	?>
</body>
</html>